/*
 * Demo of https://github.com/isuttell/sine-waves
 */

const canvas = document.querySelector('#canvas');
const c = canvas.getContext('2d');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

function Circle(x, y, radius) {
    this.x = x;
    this.y = y;
    this.vx = (Math.random() - 0.5) * 6;
    this.vy = (Math.random() - 0.5) * 6;
    this.radius = radius;

    this.draw = function() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI*2);
        c.lineWidth = 2;
        c.strokeStyle = 'rgba(2, 182, 251, 0.75)';
        c.stroke();
    }
}


function Waves() {

    this.waves = new SineWaves({
      el: canvas,
      speed: 1,
      ease: 'SineIn',
      rotate: 90,
      wavesWidth: '140%',
      width: 200,
      height: 200,
      waveLeft: 400,

      waves: [
        {
          timeModifier: 4,
          lineWidth: 1,
          amplitude: -40,
          wavelength: 40
        },
        {
          timeModifier: 2,
          lineWidth: 2,
          amplitude: -50,
          wavelength: 50
        },
        {
          timeModifier: 1,
          lineWidth: 2,
          amplitude: -50,
          wavelength: 50
        },
        {
          timeModifier: 0.5,
          lineWidth: 1,
          amplitude: -25,
          wavelength: 25
        },
        {
          timeModifier: 0.25,
          lineWidth: 2,
          amplitude: -50,
          wavelength: 50
        }
      ],

      initialize: function () {
        console.log(this.el)
        c.beginPath();
        c.arc(0, 0, 20, 0, Math.PI*2);
        c.lineWidth = 2;
        c.strokeStyle = 'rgba(2, 182, 251, 0.75)';
        c.stroke();
      },
     
      // Called on window resize
      resizeEvent: function() {
        var gradient = this.ctx.createLinearGradient(0, 0, this.width, 0);
        gradient.addColorStop(0,"rgba(1, 14, 33, 1)");
        gradient.addColorStop(0.5,"rgba(2, 182, 251, 0.5)");
        gradient.addColorStop(1,"rgba(1, 14, 33, 0)");
        
        var index = -1;
        var length = this.waves.length;
          while(++index < length){
          this.waves[index].strokeStyle = gradient;
        }
        
        // Clean Up
        index = void 0;
        length = void 0;
        gradient = void 0;


        c.beginPath();
        c.arc(10, 10, 20, 10, Math.PI*2);
        c.lineWidth = 2;
        c.strokeStyle = 'rgba(2, 182, 251, 0.75)';
        c.stroke();
      }
    });

}

var circle = new Circle(200, 200, 20);
circle.draw();

var waves = new Waves();
var waves2 = new Waves();

function animate() {
    c.clearRect(0, 0, innerWidth, innerHeight);
    circle.draw();

    requestAnimationFrame(animate)
}
// animate();
