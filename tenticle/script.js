const c = document.getElementById("c");
const ctx = c.getContext("2d");

c.width = window.innerWidth;
c.height = window.innerHeight;

function Circle(x, y, radius) {
  this.x = x;
  this.y = y;
  this.vx = (Math.random() - .5) * 0.5;
  this.vy = (Math.random() - 1) * 0.5;
  this.radius = radius;
  this.mass = this.radius;
  this.hit = false;
  this.hitTimer = 0;
  this.opacity = 0.3;

  this.draw = function() {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0.48, 2.65, true);
    ctx.closePath();
    ctx.fillStyle = 'rgba(1, 14, 33, 0.7)';
    ctx.fill();
    ctx.lineWidth = 4;
    ctx.strokeStyle = 'rgba(2, 182, 251, ' + this.opacity + ')';
    ctx.stroke();
  }

  this.update = function() {
    if(this.x + this.radius > innerWidth || this.x - this.radius < 0 ) {
      this.vx = -this.vx;
      this.hit = true;
    }
    // If this body is off the top of the screen, 
    // this values should be variables i.e. tenticleLongest
    // (might need to do this on the Jellyfish object?)
    if(this.y - (this.radius - 180) < 0) {
      this.y = innerHeight + 40;
    }
    this.x += this.vx;
    this.y += this.vy;

    this.draw();
  }

  this.hitTest = function(circle2x, circle2y) {
      var xDistance = this.x - circle2x;
      var yDistance = this.y - circle2y;

      return(Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2)));
  }
}

function Tenticle(w, pos, startPosX, startPosY, freq) {
  this.w = w;
  this.h = 10;
  this.y = 0;
  this.frequency = freq;
  this.phi = 0;
  this.frames = 0;
  this.speed = 30;
  this.opacity = 0.3;
  this.startPosX = startPosX;
  this.startPosY = startPosY;
  this.leftPos = pos;
  this.spread = Math.random() * .3;
  this.draw = function() {
    this.phi = this.frames / this.speed;
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.strokeStyle = 'rgba(2, 182, 251, ' + this.opacity + ')';
    ctx.moveTo(this.startPosX, this.startPosY);
    var spread = 2;
    for (var x = 0; x < this.w; x++) {
      var amplitude = (this.h + spread);
      y = (Math.sin(x * this.frequency + this.phi) * amplitude / 2 + amplitude / 2) + this.y;
      ctx.lineTo(y + this.leftPos + 15, this.x + x + 40); // 40 = offset
      
      spread += this.spread
    }

    ctx.stroke();
    ctx.closePath();
  }
  this.update = function() {
    this.draw();
    this.frames++;
  }
}

function Jellyfish() {
  const tenticlesLength = 6;
  var startY = Math.floor((Math.random() * 10) * 35);

  console.log("startY", startY)

  this.startPos = Math.floor((Math.random() * window.innerWidth) + 1);
  this.body = new Circle(this.startPos, window.innerHeight + startY, 26);
  this.tenticles = [];

  for (let i = 0; i < tenticlesLength; i++) {
    var tenticlesWidth = Math.floor((Math.random() * 20) + 110);
    this.tenticles.push(new Tenticle(tenticlesWidth, this.body.x, this.body.x, this.body.y + this.body.radius, 0.05))
  }

  this.animate = function() {
    this.tenticles[0].leftPos = this.body.x - 38;
    this.tenticles[1].leftPos = this.body.x - 32;
    this.tenticles[2].leftPos = this.body.x - 14;
    this.tenticles[3].leftPos = this.body.x - 28;
    this.tenticles[4].leftPos = this.body.x - 18;
    this.tenticles[5].leftPos = this.body.x - 8;

    for (let i = 0; i < this.tenticles.length; i++) {
      this.tenticles[i].x = this.body.y - this.body.radius;
      this.tenticles[i].startPosX = this.body.x;
      this.tenticles[i].startPosY = this.body.y + (this.body.radius / 2) - 3;
      this.tenticles[i].update()
    }

    if (this.body.hit == true) {
      if(this.body.hitTimer < 6) {
        this.body.opacity = 0.6
        this.body.hitTimer++;
        for (let i = 0; i < this.tenticles.length; i++) {
          this.tenticles[i].opacity = 0.5;
        }
      } else {
        this.body.hit = false;
        this.body.opacity = 0.3;
        this.body.hitTimer = 0;
        for (let i = 0; i < this.tenticles.length; i++) {
          this.tenticles[i].opacity = 0.3;
        }
      }
    }

    // Friction in movement
    if (this.body.vy > -0.2) {
      this.body.vy = -0.5
    } else {
      this.body.vy *= 0.999;
    }

    this.body.update();
  }
}

let jellyFishes = []

for(i = 0; i < 5; i++) {
  jellyFishes.push(new Jellyfish());
}

function animate() {
  ctx.clearRect(0, 0, c.width, c.height);
  for (i = 0; i < jellyFishes.length; i++) {
    jellyFishes[i].animate()
  }

  for(let i = 0; i < jellyFishes.length; i++) {
      const jellyfishA = jellyFishes[i].body;

      for(j = i + 1; j < jellyFishes.length; j++) {
          const jellyfishB = jellyFishes[j].body;
          if (jellyfishA.hitTest(jellyfishB.x, jellyfishB.y) < jellyfishA.radius + jellyfishB.radius) {

              var ax = (jellyfishA.vx * (jellyfishA.mass - jellyfishB.mass) + (2 * jellyfishB.mass * jellyfishB.vx)) / (jellyfishA.mass + jellyfishB.mass);
              var ay = (jellyfishA.vy * (jellyfishA.mass - jellyfishB.mass) + (2 * jellyfishB.mass * jellyfishB.vy)) / (jellyfishA.mass + jellyfishB.mass);
              jellyfishB.vx = (jellyfishB.vx * (jellyfishB.mass - jellyfishA.mass) + (2 * jellyfishA.mass * jellyfishA.vx)) / (jellyfishA.mass + jellyfishB.mass);
              jellyfishB.vy = (jellyfishB.vy * (jellyfishB.mass - jellyfishA.mass) + (2 * jellyfishA.mass * jellyfishA.vy)) / (jellyfishA.mass + jellyfishB.mass);
              jellyfishA.vx = ax;
              jellyfishA.vy = ay;
              jellyfishA.x = jellyfishA.x + jellyfishA.vx;
              jellyfishA.y = jellyfishA.y + jellyfishA.vy;
              jellyfishB.x = jellyfishB.x + jellyfishB.vx;
              jellyfishB.y = jellyfishB.y + jellyfishB.vy;

              jellyfishA.hit = true;
              jellyfishB.hit = true;
          }
      }

      if(jellyFishes[i].hit == true) {
        if(jellyFishes[i].body.hitTimer < 6) {
          jellyFishes[i].body.opacity = 0.6
          jellyFishes[i].body.hitTimer++;
          for (let i = 0; i < jellyFishes[i].tenticles.length; i++) {
            jellyFishes[i].tenticles[i].opacity = 0.5;
          }
        } else {
          jellyFishes[i].body.hit = false;
          jellyFishes[i].body.opacity = 0.3;
          jellyFishes[i].body.hitTimer = 0;
          for (let i = 0; i < jellyFishes[i].tenticles.length; i++) {
            jellyFishes[i].tenticles[i].opacity = 0.3;
          }
        }
      }
  }

  requestAnimationFrame(this.animate);
}
animate();

