var c = document.getElementById("c");
var ctx = c.getContext("2d");

function Tenticle() {
  this.cw = c.width = window.innerWidth;
  this.ch = c.height = window.innerHeight;
  this.cx = this.cw / 2,
  this.cy = this.ch / 2;
  this.rad = Math.PI / 180;
  this.w = 120;
  this.h = 10;
  this.amplitude = this.h;
  this.frequency = .04;
  this.phi = 0;
  this.frames = 0;
  this.stopped = true;
  this.draw = function() {
    this.frames++
    this.phi = this.frames / 30;


    ctx.clearRect(0, 0, this.cw, this.ch);
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.strokeStyle = 'rgba(2, 182, 251, 0.75)';
    ctx.moveTo(30, 40);

    // RANDOMISE THIS BETWEEN 0 AND 10
    var spread = 5;
    // amplitude += 0.1;
    // frequency += 0.000001;
    for (let x = 0; x < this.w; x++) {
      var amplitude = (this.h + spread);
      y = Math.sin(x * this.frequency + this.phi) * amplitude / 2 + amplitude / 2;
      //y = Math.cos(x * frequency + phi) * amplitude / 2 + amplitude / 2;
      ctx.lineTo(y + 30, x + 40); // 40 = offset
      
      spread += 0.2

    }
    // ctx.lineTo(w, ch);
    // ctx.lineTo(0, ch);
    ctx.stroke();


    ctx.closePath();

    ctx.beginPath();
    ctx.arc(40, 40, 20, 0, Math.PI*2);
    ctx.fillStyle = '#010e21';
    ctx.fill();
    ctx.lineWidth = 2;
    ctx.strokeStyle = 'rgba(2, 182, 251, 0.75)';
    ctx.stroke();
  }
}

var tenticle = new Tenticle();

function animate() {
  tenticle.draw();
  requestId = window.requestAnimationFrame(animate);
}

animate();