const canvas = document.querySelector('#canvas');
const c = canvas.getContext('2d');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

function Circle(x, y, radius, hsl) {
    this.x = x;
    this.y = y;
    this.vx = (Math.random() - 0.5) * 6;
    this.vy = (Math.random() - 0.5) * 6;
    this.hslOpacity = 1;
    this.hslColor = hsl + ',100%,50%';
    this.radius = radius;
    this.mass = this.radius;
    this.hitAnimating = false;
    this.hitTimer = 0;

    this.draw = function() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI*2);
        c.lineWidth = 8;
        c.strokeStyle = 'hsl(' + this.hslColor + ')';
        c.stroke();
    }
}

function Jellyfish() {
    this.circle = new Circle(200, 200, 60, 120);
    this.circle.draw();
}
    
var jf = new Jellyfish();