const canvas = document.querySelector('#canvas');
const c = canvas.getContext('2d');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

function Circle(x, y, radius, hsl) {
    this.x = x;
    this.y = y;
    this.vx = (Math.random() - 0.5) * 6;
    this.vy = (Math.random() - 0.5) * 6;
    this.hslOpacity = 1;
    this.hslColor = hsl + ',100%,50%';
    this.radius = radius;
    this.mass = this.radius;
    this.hitAnimating = false;
    this.hitTimer = 0;

    this.draw = function() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI*2);
        c.lineWidth = 8;
        c.strokeStyle = 'hsl(' + this.hslColor + ')';
        c.stroke();
    }

    this.update = function() {

        if(this.x + this.radius > innerWidth || this.x - this.radius < 0 ) {
            this.vx = -this.vx;
        }

        if(this.y + this.radius > innerHeight || this.y - this.radius < 0 ) {
            this.vy = -this.vy;
        }

        this.x += this.vx;
        this.y += this.vy;

        this.draw();
    }

    this.hitTest = function(circle2x, circle2y) {
        var xDistance = this.x - circle2x;
        var yDistance = this.y - circle2y;

        return(Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2)));
    }

    this.hitAnimation = function() {
        c.beginPath();
        c.arc(this.x, this.y, this.radius + (this.hitTimer * 1.4), 0, Math.PI*2);
        c.lineWidth = 8;
        c.strokeStyle = 'hsla(' + this.hslColor + ', ' + this.hslOpacity + ')';
        c.stroke();
        
        if (this.hitTimer < 16) {
            this.hitTimer++;
            this.hslOpacity -= 0.06
        } else {
            this.hitTimer = 0;
            this.hitAnimating = false;
            this.hslOpacity = 1;
        }
    }
}

var circles = [];

function animate() {
    c.clearRect(0, 0, innerWidth, innerHeight);
    
    for(let i = 0; i < circles.length; i++) {
        const circleA = circles[i];

        for(j = i + 1; j < circles.length; j++) {
            const circleB = circles[j];
            if (circleA.hitTest(circleB.x, circleB.y) < circleA.radius + circleB.radius) {

                var ax = (circleA.vx * (circleA.mass - circleB.mass) + (2 * circleB.mass * circleB.vx)) / (circleA.mass + circleB.mass);
                var ay = (circleA.vy * (circleA.mass - circleB.mass) + (2 * circleB.mass * circleB.vy)) / (circleA.mass + circleB.mass);
                circleB.vx = (circleB.vx * (circleB.mass - circleA.mass) + (2 * circleA.mass * circleA.vx)) / (circleA.mass + circleB.mass);
                circleB.vy = (circleB.vy * (circleB.mass - circleA.mass) + (2 * circleA.mass * circleA.vy)) / (circleA.mass + circleB.mass);
                circleA.vx = ax;
                circleA.vy = ay;
                circleA.x = circleA.x + circleA.vx;
                circleA.y = circleA.y + circleA.vy;
                circleB.x = circleB.x + circleB.vx;
                circleB.y = circleB.y + circleB.vy;

                circleA.hitAnimating = true;
                circleB.hitAnimating = true;
            }
        }

        if(circles[i].hitAnimating == true) {
            circleA.hitAnimation();
        }

        circles[i].update();

    }

    requestAnimationFrame(animate)
}

(function init() {
    for(let i = 0; i < 12; i++) {
        const radius = 32;
        let x = Math.random() * (innerWidth - radius * 2) + radius;
        let y = Math.random() * (innerHeight - radius * 2) + radius;
        var hue = Math.random() * 360;

        for(let j = 0; j < circles.length; j++) {
            if(x + radius > circles[j].x && x - radius < circles[j].x) {
                x += radius + 1;
            }

            if(y + radius > circles[j].y && y - radius < circles[j].y) {
                y += radius + 1;
            }
        }

        circles.push (new Circle(x, y, radius, hue))
    }
    
    animate();
})()
